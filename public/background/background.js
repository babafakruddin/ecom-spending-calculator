
chrome.runtime.onInstalled.addListener(details => {
  if(details.reason == chrome.runtime.OnInstalledReason.INSTALL)
  {
  chrome.notifications.create(
    {
        type: "basic",
        iconUrl: chrome.runtime.getURL('Icons/icon 32.png'),
        title: "Hey 😃  e-shopper!",
        message: "Thanks for installing Ecom Spending Calculator!",
        silent: false
    },
    () => { 
    }
)
  }
});
