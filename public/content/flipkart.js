//Final Data array for popup
let amazonOrderedItems =[]

// Basically used Variables
let initialData
let tempdata
let pageCount
let orderLimit
let dataYear = []
let forYear
let lastEle
// let lastPageCount
let earlierCount = 0
let allPageCount = 0
let totalPagesFetched=0
// Variables for Amazon data processing

let nameFinal
let nameEv
let getPrice
let orderedDate 
let orderdId
let curObj
// let c=0
/**
 * @description Fetches each page with generated link
 * @param {*} page 
 * @returns DOM
 */
let FetchApi = (page) => {
  return new Promise((resolve, reject) => {
    fetch(page)
      .then((res) => res.text())
      .then((data) => {
        
        initialData=data
        const parser = new DOMParser()
        let dom = parser.parseFromString(initialData, "text/html");

  		//Processing the dom for each page of an year
          // if(dom){
          //     c+=1
          // }
          // console.log(c)
        findAmazonOrders(dom)
        
        resolve(data);
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};

//For Getting the invoice of top 5 orders
// https://www.amazon.in/gp/css/summary/print.html/ref=oh_aui_ajax_invoice?ie=UTF8&orderID=D01-2636112-4213461

/**
 *@description Extracts Years in the account and pushes all years into dataYear array
 */
const getAllYears = ()=>{
  const ele =document.querySelector('[name=orderFilter]')
  for(let i=0;i<ele.options.length;i++){
    let data =(ele.options[i].innerText).replace(/\s+/g,'')
      if(!isNaN(data)){
   dataYear.push(Number(data))
      }
 }
 generatePage(dataYear)
console.log(dataYear)
lastEle = dataYear.at(-1)
}

//Get all the year details in the order history


/**
 * @description Generating links for all years and calls FetchApi function
 * @param {*} dataYear  
 */
const generatePage = (dataYear)=>{
  dataYear.map((ele,index)=>{
    orderLimit=0
    yearPagesFetching =`https://www.amazon.in/gp/css/order-history?opt=ab&digitalOrders=1&unifiedOrders=1&returnTo=&orderFilter=year-${ele}&startIndex=0&language=en_GB&disableCsd=no-js`

  fetch(yearPagesFetching)
  .then((response) => response.text())
  .then((data) =>{

   forYear=data
   const parser = new DOMParser()
   tempdata = parser.parseFromString(forYear, "text/html")
   let a =  tempdata.getElementsByClassName("a-pagination")[0]

    // Edge Case if we don't have any Pagination in the orders page .... Gautam account 2015 year
   if(!a){
    FetchApi(`https://www.amazon.in/gp/css/order-history?opt=ab&digitalOrders=1&unifiedOrders=1&returnTo=&orderFilter=year-${ele}&startIndex=0&language=en_GB&disableCsd=no-js`)
	totalPagesFetched+=1
	
   //  if(a.outerText!==undefined){
   //        let aa= a.outerText.split("\n")
   //         pageCount =Number(aa.at(-2))
   //             console.log(pageCount)
    
   // }
    //    earlierCount=allPageCount
    // allPageCount+=1
       
       // console.log(allPageCount)
   }
if(a!=undefined){
           if(a.outerText!==undefined){
          let aa= a.outerText.split("\n")
           pageCount =Number(aa.at(-2))
               // console.log(pageCount)
               // console.log(allPageCount)
            if(pageCount!=NaN){
        earlierCount = allPageCount  
         allPageCount+= pageCount
                
        
     }
      }
      }
      // console.log(earlierCount)
      //if(ele===lastEle){
         // lastPageCount = pageCount
    //  }
    
      if(allPageCount!==earlierCount && ele===lastEle){
          // console.log(allPageCount)
         totalPagesFetched += allPageCount
         console.log(totalPagesFetched) 
      }
 
   for(let i=0;i<pageCount;i++){ 
    
    let url = `https://www.amazon.in/gp/css/order-history?opt=ab&digitalOrders=1&unifiedOrders=1&returnTo=&orderFilter=year-${ele}&startIndex=${orderLimit}&language=en_GB&disableCsd=no-js`
   	//console.log(url)
    FetchApi(url)
    orderLimit+=10

  }
  orderLimit=0
  });
	
})
    
}

/**
 * @description Generates object with the details 
 * @param {*} itemPrice 
 * @param {*} year 
 * @param {*} name 
 * @param {*} id 
 * @returns object
 */
const orderDetails =async (itemPrice, year, name,id) => {
  let obj = {};
  obj.price = itemPrice;
  obj.year = year;
  obj.name = name
  obj.orderId = id
  
  return obj;
};

/**
 * @description From the virutal DOM that is created it extracts data from the dom and pushes into amazonOrderedItems array 
 * @param {*} dom 
 */
let domCount=0
const findAmazonOrders =async (dom) =>{
	
	// if(dom){
	domCount+=1
// }
	// console.log(domCount)
	const ordersElement = dom.querySelectorAll(".js-order-card")
	
    // let check=dom.getElementsByClassName("a-pagination")[0]
    
    //  if(check!=undefined){
    //        if(check.outerText!==undefined){
    //       let aa= check.outerText.split("\n")
    //        let bb =Number(aa.at(-2))
               
    // console.log(bb)
            
    //   }
    //   }
      
    //let status = dom.getElementsByClassName("a-last")[0].outerHTML.includes("disabled")
    //console.log(status)
     
  	for(let index=0;index<ordersElement.length;index++){
  		let flag = true
  		
		let  dataOfOneOrder = ordersElement[index]

		let priceElement = dataOfOneOrder.getElementsByClassName("yohtmlc-order-total")

		let dateElement= dataOfOneOrder.getElementsByClassName("a-fixed-right-grid-col a-col-left")[0] 
		dateElement = dateElement.querySelector(".a-color-secondary.value")
		 
		let nameE =dataOfOneOrder.querySelectorAll(".a-fixed-left-grid-col.yohtmlc-item.a-col-right")[0]
		let orderIdElement = dataOfOneOrder.getElementsByClassName("yohtmlc-order-id")
		
		// For getting Delivery status of an element
		let deliveryStatus = dataOfOneOrder.getElementsByClassName("js-shipment-info-container")[0]
		
		if(deliveryStatus!==undefined){
            let arr= ["PaymentFailed","Returncomplete","Refunded","Cancelled","Refundissued"
,"Closedduetolackofpayment","Returnreceived"] 

            let t = deliveryStatus.innerText.split("\n")//[0]
            let s = String(t)
          	s.replace(/\s+/g,'').trim().replace(/,/g, "")
            s= String(s).replace(/\s+/g,',','').trim().replace(/,/g, "")
          
          //Checking whether order Status matches with our conditions
            arr.map((e)=>{
                if(s.includes(e)){
                    flag = false   
                }
            })
        }
        
      	//Only delivered items will be processed 
      	
        if(flag){
            if(nameE===undefined){
		 	nameE = dataOfOneOrder.querySelectorAll(".a-size-medium.a-text-bold")[0]
			nameFinal = nameE.innerText.replace('\n','').trim()
			
			//Class Name for Amazon Fresh orders. Some orders are not having name for handling that
		  let temp =dataOfOneOrder.getElementsByClassName("a-unordered-list a-nostyle a-horizontal a-spacing-extra-large")
		  
		  //For handling Amazon Fresh Orders
		   if(nameFinal.includes("Fresh") || temp!=null){
				 nameFinal = " "
		   }
		   
		}
		
		//For getting names of the orders through link
		
		nameEv = nameE.querySelector(".a-row")
		
		if(nameEv !=null){
			nameFinal = nameE.innerText.replace('\n','').trim()
		} 
		nameFinal = nameFinal.split("\n")[0]
		
		// Total price of the order
		getPrice =priceElement[0].innerText.replace(/\s+/g,'').trim()
		
		//Handling Audible subscription 
		if(getPrice==="Total1AudibleCredit"){
		  	getPrice = 0
		}
        
		else{
			getPrice = Number(getPrice.split(".")[1].replace(",",''))
		 	//getPrice =Number(getPrice.split('.')[0].replace(/\,/g,'').match(/\d+/g)[0])
		}
	 
	 // Ordered Date of an order
		orderedDate = dateElement.innerText
		orderedDate=orderedDate.replace(/\s+/g,'').trim()
		orderedDate = orderedDate.match(/\d+/g)[1]
		
		if(orderedDate === undefined){
			orderedDate = new Date().getFullYear()
		}

	//OrderdId of an Order
		orderdId = orderIdElement[0].innerText
		orderdId=orderdId.replace(/\s+/g,'').trim().split("#")[1]
		
	// Creating order Object
        curObj = await orderDetails(getPrice,orderedDate,nameFinal,orderdId)
		amazonOrderedItems.push(curObj)
        }
    }
    
    if(domCount===totalPagesFetched){
   	console.log(amazonOrderedItems)
       //////////////////////////Message Sending Condition////////////////////////
       chrome.storage.local.set({ AmazonData: amazonOrderedItems })

 

   
         chrome.runtime.sendMessage(
           { message: "User-List", amazonOrderedItems },
           function (response) { }
         );
 
       
   	}
}

// First function to be executed for getting all the years to fetch orders

// First function to be executed for getting all the years to fetch orders



















const data=()=>
{
   let FlipOrderedItems=[]
const orderDetails = (itemPrice, year, name) => {

  let obj = {};
  obj.price = itemPrice;
  obj.year = year;
  obj.name = name
  

  return obj;
};
const getyear=(s)=>{

    if(s.includes("Delivered") || s.includes("Arriving") || s.includes("Delivery"))
{
    const ar=s.split(" ")
    //console.log(ar)
   
    if(ar[4]==undefined ) 
    {
        return new Date().getFullYear()  
    }
    else
    {
      if(+ar[4] <50 || ar[4].includes("AM")){
        return new Date().getFullYear()  
                
            }
            else{
            
                y=+ar[4] ? ar[4] : " "
                return y
            }
    }

}


}
const GetCost=(cost)=>
{
    
    cost=cost.replace(/\,/g,'');
   // console.log(cost.match(/(\d+)/)[0])
    return matches = cost.match(/(\d+)/)[0];
    
}

const all=document.getElementsByClassName("_35va4c")

for(var l=0 ; l<all.length ;l++)
    {
        let status,name,cost
        const arr=all[l].outerText.split("\n")
      
        for(var i=0;i<arr.length ;i++){
          
          name=arr[0].includes("shared") ? arr[1]  :arr[0]
         if(arr[i].includes("Delivered") || arr[i].includes("Cancelled") || arr[i].includes("Refund") ||  arr[i].includes("Arriving ") || arr[i].includes("Delivery") || arr[i].includes("Cancellation requested"))
        {
             status=arr[i]
        }
        if(arr[i].includes("₹") )
        {
            //console.log(arr[i])
            cost=arr[i]
        }
        }
    
          if(getyear(status)){
         curObj =  orderDetails(GetCost(cost),getyear(status),name)
		FlipOrderedItems.push(curObj)
            
       }
       chrome.storage.local.set({ FlipData: FlipOrderedItems });
             chrome.runtime.sendMessage(
               { message: "Flip-List", FlipOrderedItems },
               function (response) { }
             ); 
   
    
       
    }
 //console.log(FlipOrderedItems)

}

const  pageScroll= ()=> {  
    window.scrollBy(0, 1400); 
    scrolldelay = setTimeout('pageScroll()',2000);      
       
    var classes = document.getElementsByClassName('_2KpZ6l _2jekE6');
    var Rate = classes[0];
      try {
          if(Rate)
          {
    Rate.click()
          // data()
          
          }   
          else{
            if(document.getElementsByClassName("_13P_9S")[0].outerText.includes("No"))
            {
             console.log("entered")
             data()
            
             clearTimeout(scrolldelay)
           //  data()
            }
          }    
        }
        catch(err) {
        //  data()
        console.log("eroor")
        }      
  }


//Message sending and recieving
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.message == "start-analyze") {
  
     currentPage = window.location.href;
    if (currentPage.includes("flipkart.com/account/orders?link=home_orders")) {

        pageScroll()
    }
    else if (currentPage.includes("amazon.in")) {
      amazonOrderedItems=[]
      dataYear = []
      totalPagesFetched=0
DomCount=0
    getAllYears()
     
      // generatePage(dataYear)
    

      
    }
   
      
    
  }
  
});


// btnExportToCsv.addEventListener("click", () => {
//   const exporter = new TableCSVExporter(FlipOrderedItems);
//   const csvOutput = exporter.convertToCSV();
//   const csvBlob = new Blob([csvOutput], { type: "text/csv" });
//   const blobUrl = URL.createObjectURL(csvBlob);
//   const anchorElement = document.createElement("a");

//   anchorElement.href = blobUrl;
//   anchorElement.download = "table-export.csv";
//   anchorElement.click();

//   setTimeout(() => {
//       URL.revokeObjectURL(blobUrl);
//   }, 500);
// });