import React, { useState, useEffect } from "react";
import { CSVLink } from "react-csv";
import {
  DishListS,
  AmountContainer,
  TotalAmountHeading,
  AnalyzeButton,
  TotalAmount,
  LeftSvg,
  RightSvg,
  InstructionHeadingWrapper,
  InstructitopDishonHeadingWrapper,
  InstructionContainer,
  Instructions,
  Ruler,
  Csvbutton,
  Buttondata,
  CurrentTabContainer
} from "./index.styled";
import LeftVector from "./../../assets/img/vector-left.svg";
import RightVector from "./../../assets/img/vector-right.svg";
import NotesIcon from "./../../assets/img/NotesIcon.svg";
import DishIcon from "./../../assets/img/cutlery 1.svg";
import ParseLoader from "./../../assets/img/Pulse-1s-200px.gif"
import AmazonImg from "./../../assets/img/Amazon.svg"
import FlipkartImg from "./../../assets/img/Flipkart.svg"
import Medal from "./../../assets/img/medal.svg"
const Body = ({ 
  Dish,
  totalAmount,
  isLoading,
  setIsLoading,
  disabled,
  setDisabled,
    Tab,
    data
 
}) => {

  
  const styles = {
    buttonDisabled: {
      padding: '5px 10px',
      cursor: 'not-allowed',
      
    },
  };
 

  //first function when we click analyse button and send message to content
  const handleAnalyse = (e) => {
    setDisabled(true)
    setIsLoading(true);
    e.preventDefault();
    window.chrome.tabs.query(
      {
        active: true,
        currentWindow: true,
      },
      function (tabs) {
        var activeTab = tabs[0];
        window.chrome.tabs.sendMessage(
          activeTab.id,
          {
            message: "start-analyze",
            
          },
          function (response) {}
          
        );
       
      }
    );
   
  };
 
 const DownloadData=(e)=>
 {
  

  console.log("logic to convert csv ")
  
  
  
 }
 
  useEffect(() => {
    
  }, [Dish])
  
  return (
    <>
      <AmountContainer>
        <LeftSvg src={LeftVector} />
        <CurrentTabContainer>      
       {
        Tab.includes("amazon.in") ? <img src={AmazonImg} /> : Tab.includes("flipkart.com") ? <img src={FlipkartImg} /> : " "
       }
        </CurrentTabContainer>
        <TotalAmountHeading>Total Amount Spent</TotalAmountHeading>

        <TotalAmount>
          {totalAmount ? totalAmount : "₹ _ _ _ _ . _ _"}
        </TotalAmount>

        <AnalyzeButton disabled={disabled} onClick={handleAnalyse}

         style={disabled  ? styles.buttonDisabled : styles.button}
        >
          {isLoading && isLoading  ? Tab.includes("amazon.in") || Tab.includes("flipkart.com") ? <Buttondata>{"Fetching-"} <img src={ParseLoader} />  </Buttondata>:"Sorry open amazon/flipkart " :  Tab.includes("amazon.in") || Tab.includes("flipkart.com") ?  Dish.length> 0? "Re-Analyze Total Spending" : "Analyze Total Spending " : "Please open Flipkart/Amazon"  }{" "}
        </AnalyzeButton>
        <RightSvg src={RightVector} />
      </AmountContainer>
       {/* <Csvbutton onClick={DownloadData} >
       Export to CSV
       </Csvbutton> */}
      <InstructionContainer>
      <Ruler/>
        {Dish.length > 0 ? ( 
          <>
           <InstructitopDishonHeadingWrapper><h3>Most Purchased Product</h3>

              <img src={DishIcon} /> </InstructitopDishonHeadingWrapper>
            {Dish.map((i,index)=>
            {
             return  <DishListS><p>{index+1}{". "}{i.includes("More") ? "Grocery items" : i }{index==0 ? <img src={Medal} /> :""}</p></DishListS>          
            })}
          </>
        ) 
        : 
        (
          <>
            <InstructionHeadingWrapper>
              <h3>Instruction</h3>
              <img src={NotesIcon} />
            </InstructionHeadingWrapper>
            <Instructions>
              <p>1. Open the Flipkart/Amazon website</p>
              <p>2. Sign in to your account</p>
              <p>3. Go to the "My Order" section & click the extension icon</p>
              <p>4. In the final step, click "Analyze total spending" to get the total expenditure</p>
            </Instructions>
          </>
        )}
      </InstructionContainer>
    </>
  );
};

export default Body;
