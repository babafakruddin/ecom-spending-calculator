import styled from 'styled-components'

export const AmountContainer = styled.div`
    background: #E9EDDE;
    border-radius: 10px;
    height: 180px;
    margin-left: 17px;
    margin-top: 10px;
    width: 366px;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    
`

export const TotalAmountHeading = styled.h3`
    color: #414141;
    font-family: 'Rubik';
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
`;



export const AnalyzeButton = styled.button`
        width: auto;
    padding: 10px;

    height: 35px;
    background: #5A5E51;
    border-radius: 4px;
    border: none;
    outline: none;
    font-family: 'Inter';
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    color: #FFFFFF;
    margin-top: 20px;
    cursor: pointer;
   
    
`;


export const Buttondata=styled.div` 
     width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-evenly;
     span{
        
     }
      img{
        width: 40px;
        height: 20px;
    }
     
`


export const TotalAmount = styled.h3`
    font-family: 'Rubik';
    font-style: normal;
    font-weight: 600;
    font-size: 22px;
    color: #000000;
    margin-top: 7px;
`

export const LeftSvg = styled.img`
    position: absolute;
    left: 0;
    top: 0;
`

export const RightSvg = styled.img`
    position: absolute;
    bottom: 0;
    right: 0;
`

export const Ruler = styled.div`
    border: 0.25px solid rgb(204 204 204 / 47%);
    width: 183px;
    
    margin-bottom: 10px;

    
`

export const InstructionContainer = styled.div`
    width: 366px;
    height: 260px;
    padding: 13px 23px;
    margin-left: 17px;
    margin-top: 20px;
    background-color: #F9F9F9;
    img{
        width: 18px;
        height: 18px;
        margin-left: 6px;
        margin-bottom: 6px;
    }
`

export const Instructions = styled.div`
    margin-top: 5px;
    p{
        font-family: 'Rubik';
        font-weight: 400;
        font-size: 14px;
        color: #414141;
        padding: 4px 0px;
        display: flex;
    align-items: center;
    line-height: 22px;
    }
`
export const DishListS = styled.div`
    margin-top: 5px;
    p{
        font-family: 'Rubik';
        font-weight: 400;
        font-size: 14px;
        color: #414141;
        padding: 1px 0px;

    display: flex;
    align-items: center;
    line-height: 22px;
    }
`

export const InstructionHeadingWrapper = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
    h3{
        margin-bottom: 5px;
    }
`
export const InstructitopDishonHeadingWrapper =styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
    h3{
        
        margin-bottom: 5px;
    }
    

`
export const   CurrentTabContainer =styled.div`

    display: flex;
    align-items: center;
    
    padding: 2px 15px 15px;
    img
    {
        width: 88px;
        height: 45px;
    }

`
