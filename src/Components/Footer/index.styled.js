import styled from 'styled-components'

export const FooterContainer = styled.div`
    display: flex;
    justify-content: end;
    margin-right: 13px;
    padding-bottom: 10px;
    text-decoration: none;
   
    .split-line{
        margin: 0px 10px;
    }
`

export const RateUs = styled.div`
   font-family: 'Inter';
    font-weight: 400;
    font-size: 14px;
    color: #251605;
    text-decoration: none;
    img{
        margin-right: 2px;
    }
`

export const Feedback = styled.div`
    font-family: 'Inter';
    font-weight: 400;
    font-size: 14px;
    color: #251605;
    text-decoration: none;
    img{
        margin-right: 2px;
    }
`