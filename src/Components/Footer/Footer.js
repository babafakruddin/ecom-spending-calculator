import React from 'react'
import {BrowserRouter as Router, Link} from 'react-router-dom';

import StarIcon from './../../assets/img/Star.svg'
import FeedbackIcon from './../../assets/img/Feedback.svg'
import Line from './../../assets/img/Line.svg'

import { FooterContainer, RateUs, Feedback} from "./index.styled"

const Footer = () => {
  const MyStyle={
    textDecoration: 'none'
  }
  return (
  
    <FooterContainer>
       <Router>
       <a style={MyStyle} href="https://chrome.google.com/webstore/detail/spending-calculator-for-s/dbbbhmnphepimpameepigkpjjnlpmjeg/reviews/" target="_blank" rel="noreferrer">
      <RateUs>
     
        <img src={StarIcon}/> Rate us
        
      </RateUs>
      </a>
      <img className='split-line' src={Line} />

      <a style={MyStyle} href="https://bit.ly/spenuin" target="_blank" rel="noreferrer">
      <Feedback>
        <img src={FeedbackIcon}/> Feedback
      </Feedback>
      </a>
      </Router>
    </FooterContainer>
  )
}

export default Footer;