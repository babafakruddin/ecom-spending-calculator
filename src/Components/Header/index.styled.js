import styled from 'styled-components'

export const LogoWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    padding: 14px;
    select{
        width: 67px;
        height: 25px;
        border: 1px solid rgb(211 172 27);
        border-radius: 5px;
        font-weight: 400;
        font-size: 12px;
        color: rgb(16 15 16);

        font-family: Inter, sans-serif;
        cursor: pointer;
    }
`;

export const LogContainer = styled.div`
    img{
        height: 42px;
    width: 102px;
    }
`;
