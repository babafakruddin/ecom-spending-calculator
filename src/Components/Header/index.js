
import React,{useState} from 'react'
import companyLogo from '../../assets/img/Logo.svg';
import {LogoWrapper, LogContainer, Title} from "./index.styled"


const Header = ({yearData, processDataCal}) => {
    const handleYearSelect=(e)=>
    {
       
        processDataCal( e.target.value);
        
    }

    return <LogoWrapper>
                <LogContainer>
                    <img src={companyLogo} alt=". logo"/>
                </LogContainer>
            
                {yearData.length > 0  && <select id="years" name="years" onChange={handleYearSelect} >
                    <option value="All" >All</option>
                    { yearData.map((i) =>{
                       return <option value={i} >{i}</option>
                    }
                    )}
                </select> }
                
               
            </LogoWrapper>
     
}

export default Header;