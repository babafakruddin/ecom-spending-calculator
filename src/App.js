import React, { useState, useEffect } from "react";
import "./App.css";
import { CSVLink } from "react-csv";
import { GlobalStyle } from "./globalStyles";
import Header from "./Components/Header";
import Body from "./Components/Body";
import Footer from "./Components/Footer/Footer";
import { json } from "react-router-dom";

function App() {
  const [topDish, setTopDish] = useState([]);
  const [data, setData] = useState([]);
  const [yearData, setYearData] = useState([]);
  const [totalAmount, setTotalAmount] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [tab, SetTab] = useState("");

//calculate top five dishes and total amount
  const processDataCal = (year, userData) => {
    let dataToMap = userData ? userData : data;
    let total = 0;
    let s = "";

    dataToMap.map((order) => {
      if (order.year == year || year == "All") {
       
        if (!isNaN(order.price)) {
          total += Number(order.price);
        } else {
          total += order.price;
        }
      //  console.log(order.name)
        
          const a = order.name
         
         
            s += '\n'+a
          //  console.log(s)
        
        
      }
    });
    let arr = s.split("\n");
   // console.log(arr)
    let hashMap = {};
    for (let i = 0; i < arr.length; i++) {
      if (!hashMap[arr[i]]) {
        hashMap[arr[i]] = 1;
      } else {
        hashMap[arr[i]] += 1;
      }
    }

    const sortable = Object.entries(hashMap)
      .sort(([, a], [, b]) => b - a)
      .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

    const sliced = Object.fromEntries(Object.entries(sortable).slice(0, 4));
    let DishList = [];
    for (let e1 in sliced) {
      if (e1.trim().length === 0) {
        console.log("This is an empty string!");
      } else {
        DishList.push(e1);
        setTopDish(DishList);
      }
      setTopDish(DishList);
      setDisabled(false);
    }
    setTotalAmount(total.toLocaleString('en-IN', {
      maximumFractionDigits: 2,
      style: 'currency',
      currency: 'INR'
  }))
    setIsLoading(false);
  };
  
  let currentTab;
  let arr=[];


//Get and store data from localStorage
  useEffect(() => {
    window.chrome.tabs.query(
      { active: true, currentWindow: true },
      function (tabs) {
        currentTab = tabs[0];
        SetTab(currentTab.url);
        if (currentTab.url.includes("amazon.in")) {
          window.chrome.storage.local.get(["AmazonData"]).then((result) => {
            arr=result.AmazonData
            console.log(arr)
            setData(arr);
            processData(arr);
            processDataCal("All", arr);
            setIsLoading(false);
          });
         
        } 
        
        else if (currentTab.url.includes("flipkart.com")) {
          window.chrome.storage.local.get(["FlipData"]).then((result) => {
            arr = result.FlipData
            setData(arr);
            processData(arr);
            processDataCal("All", arr);
            setIsLoading(false);
           
           });
        }
      }
    );
    
  }, []);
  let a = [];
  //getting final userlist from zomato or ordereditems from Swiggy
  window.chrome.runtime.onMessage.addListener(function (
    request,
    sender,
    sendResponse
  ) {
    if (request.message == "User-List") {
    //  console.log(request.amazonOrderedItems)
      setData(request.amazonOrderedItems);
      processData(request.amazonOrderedItems);
      processDataCal("All", request.amazonOrderedItems);
      setIsLoading(false);
    }
    if (request.message == "Flip-List") {
     // console.log(request.FlipOrderedItems)
      setData(request.FlipOrderedItems);
      processData(request.FlipOrderedItems);
      processDataCal("All", request.FlipOrderedItems);
      setIsLoading(false);
    }

  });
//Getting YearList
  const processData = async (d) => {
    let yearList = [];

    d.map((order) => {
      if (!yearList.includes(order.year)) {
        yearList.push(order.year);
      }
    });
   ;
    setYearData( yearList.sort(function(a, b){return b-a}));
  };
  // const headers=[
  //   {
  //     label:"name" ,key:"name"
      
  //   },
  //   {
  //     label:"price" ,key:"price"
  //   },
  //   {
  //     label:"year",key:"year"
  //   }
    
  // ]
  // const csvLink={
  //     headers:headers,
  //     data:data,
  //     filename:"csvfile.csv"
  // }
  return (
    <>
    
      <GlobalStyle />
      <Header yearData={yearData} processDataCal={processDataCal} />

      <Body
        Dish={topDish}export default DataForm
        setTopDish={setTopDish}
        totalAmount={totalAmount}
        yearData={yearData}
        setTotalAmount={setTotalAmount}
        Totalcal={processDataCal}
        isLoading={isLoading}
        setIsLoading={setIsLoading}
        setYearData={setYearData}
        disabled={disabled}
        setDisabled={setDisabled}
        Tab={tab}
        data={data}
      />

      {/* <CSVLink {...csvLink}>click here</CSVLink> */}
    
      <Footer />
    </>
  );
}

export default App;
